package com.example.ball.dx.dxball;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

import android.support.constraint.solver.widgets.Rectangle;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import static java.lang.Math.abs;
import static java.lang.Thread.sleep;


/**
 * Created by Farzad on 11/30/2017.
 */

public class Ball  implements Runnable {

    int x,y,r,dx,dy;
    int height,width,speed=4;
    Paint paint;
    Map position;
    public boolean run=false,still=false;

    public Ball(int x, int y, int r){


        this.x=x;
        this.y=y;
        this.r=r;
        position=new HashMap<String, Float>();


        paint=new Paint();

        paint.setColor(Color.RED);
        paint.setStyle(Paint.Style.FILL);

        dy=speed/2;
        dx=speed/2;

        // Important for certain APIs
        // paint.setLayerType(LAYER_TYPE_SOFTWARE, paint);





        height= Resources.getSystem().getDisplayMetrics().heightPixels;
        width=Resources.getSystem().getDisplayMetrics().widthPixels;


    }

    public void calculateBorder(){

        //left border
        if(x+1<=r){
            dx=-dx;

        }



        //Top border

        else if(y-Board.topBar.bottom<=r){
            dy=-dy;

        }



        //right border

        else if(width-1<=x+r){
            dx=-dx;


        }

        //Bottom border

        else if(height-1<=y+r){
            dy=-dy;

            Score.life-=1;
            MainActivity.sp.dead();


            dy=0;
            dx=0;
            still=true;



        }


    }


    public void calculateBar(){
        if(y+r>=Bar.b.y && x+r>=Bar.b.x && x-r<=Bar.b.x+Bar.b.width &&  y - r <=Bar.b.y+Bar.b.height){
            float temp=Bar.b.x+Bar.b.width/2;
            Log.d("temp", String.valueOf(temp));
            Log.d("temp3", String.valueOf(x));

            //at center
            if(x>=temp-10 && x<=temp+10){
                dx=0;
                dy=(int)(speed*80/100);
            }
            else if (x>temp-30 && x<temp){
                dx=-2;
                dy=speed-abs(dx);
            }
            else if(x<temp){
                Log.d("temp2", "colide left");
                dx=-speed/2;
                dy=speed-abs(dx);
            }
            else if (x<temp+30 && x>temp){
                dx=2;
                dy=speed-abs(dx);
            }
            else if(x>temp){
                Log.d("temp2", "colide right");
                dx=speed/2;
                dy=speed-abs(dx);
            }

            dy=-dy;

        }

    }


    /*public void run() {

        x+=dx; y+=dy;
        calculateBorder();
        calculateBar();


    }*/


    public Map getPostion() {
        position.put("x",x);
        position.put("y",y);
        position.put("r",r);


        return position;
    }

    public boolean randomBoolean(){
        return Math.random() < 0.5;
    }



    public boolean colidesWith(int left,int top,int right,int bottom) {
        //boolean colide=false;

        if (x + r >= left &&
                x - r<= right &&
                y - r <= bottom &&
                y + r >= top)
        {

            dy=-dy;

            if(randomBoolean()){
                Log.d("change","true");
                dx=-dx;

            }
            else {
                Log.d("change","false");

            }


            return true;

        }




        return false;

    }








    public Paint getPaint() {
        return paint;
    }
    int i=0;

    @Override
    public void run() {

        while (run){


            try {
                if(still){
                    Thread thread = new Thread() {
                        @Override
                        public void run() {
                            try {
                                still=false;
                                sleep(2000);
                                dx=speed/2;
                                dy=-speed/2;

                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    };

                    thread.start();


                }
                i++;
                if(i<60){

                    paint.setColor(Color.DKGRAY);}
                else if(i<120){
                    paint.setColor(Color.RED);
                }

                else if(i<150){
                    i=0;
                }

                if(dx==0 && dy==0) {

                    x= (int) Bar.b.x+160;
                    y = (int) Bar.b.y-r;


                }
                else  {

                    sleep(5);

                    x+=dx; y+=dy;
                    calculateBorder();
                    calculateBar(); }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if(MainActivity.pause || Score.gameOver){
                run=false;
            }


        }



    }
}
