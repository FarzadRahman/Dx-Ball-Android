package com.example.ball.dx.dxball;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v4.view.MotionEventCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.util.Timer;
import java.util.TimerTask;


public class MainActivity extends Activity implements SensorEventListener {

    static boolean pause=false;
    static int view;
    static SharedPreferences sharedpreferences;
    //static int highScore;
    TextView highScore;

    Dialog myDialogue;


    private SensorManager senSensorManager;
    private Sensor senAccelerometer;
    public static SoundPlayer sp;






    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //to hide title bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        view=0;
        setContentView(R.layout.activity_main);

        highScore=(TextView)findViewById(R.id.high_score);

        MainActivity.sharedpreferences = getSharedPreferences("game", Context.MODE_PRIVATE);
        MainActivity.sharedpreferences = getSharedPreferences("stage", Context.MODE_PRIVATE);
        MainActivity.sharedpreferences = getSharedPreferences("sensor", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = MainActivity.sharedpreferences.edit();


        int s= MainActivity.sharedpreferences.getInt("sensor",0);
        Log.d("Sensor",String.valueOf(s));



        senSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        senAccelerometer = senSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);




    }

    @Override
    public void onBackPressed() {


        if(view==0){
            exit();
        }
        else if(view==1){
            if(MainActivity.pause==false){
                if(!Score.gameOver)
                {MainActivity.pause=true;}
            }
            else if(MainActivity.pause==true){
                MainActivity.pause=false;
            }

        }



    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Score.gameOver=true;
        senSensorManager.unregisterListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();

        int defaultValue = MainActivity.sharedpreferences.getInt("game",0);
        highScore.setText("High Score : "+String.valueOf(defaultValue));


        int complete=MainActivity.sharedpreferences.getInt("game",0);
        Log.d("stage",String.valueOf(complete));

        complete=MainActivity.sharedpreferences.getInt("stage",0);

        Log.d("stage",String.valueOf(complete));


        myDialogue=new Dialog(this);
        myDialogue.setContentView(R.layout.settings);
        myDialogue.setTitle("Settings");




        sp=new SoundPlayer(this);



    }

    public void exit(){

        finish();

    }


    @Override
    public boolean onTouchEvent(MotionEvent event){




        if(view!=0){

            if(Board.showMenuButton){
                //setContentView(R.layout.activity_main);
                if(event.getAction()==MotionEvent.ACTION_UP) {
                    Intent intent = getIntent();
                    finish();
                    startActivity(intent);
                    Board.showMenuButton = false;
                    Score.score=0;

                }

            }


        float x=event.getX();
        float y=event.getY();
        Bar.touch(x,y);




        }



        return true;
    }


    @Override
    protected void onResume() {
        super.onResume();


        sp.play();
        int s= MainActivity.sharedpreferences.getInt("sensor",0);
        if(s==1){
            senSensorManager.registerListener(this, senAccelerometer , SensorManager.SENSOR_DELAY_NORMAL);
        }


    }

    public void startGame(View v){
        Button btn=(Button)v;
        if(btn.getText().toString().equals("Exit")){ exit(); }

        else if(btn.getText().toString().equals("Start")){
           Board b=new Board(this);
            view=1;
            setContentView(b);
        }

        else if(btn.getText().toString().equals("Settings")){

           showSettings();

        }
    }



    public void showSettings(){
        TextView cross=(TextView)myDialogue.findViewById(R.id.cross);
        Button rScore=(Button)myDialogue.findViewById(R.id.resetScore);
        Button rLevel=(Button)myDialogue.findViewById(R.id.resetLevel);
        final Button sensor=(Button)myDialogue.findViewById(R.id.sensor);

        int s= MainActivity.sharedpreferences.getInt("sensor",0);
        Log.d("Sensor",String.valueOf(s));
        if(s==0){

            sensor.setText("Sensor is Off");
        }
        else if(s==1){

            sensor.setText("Sensor is On");
        }




        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myDialogue.cancel();
            }
        });

        rScore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = MainActivity.sharedpreferences.edit();
                editor.putInt("game",0);
                editor.commit();
                highScore.setText("High Score : 0");

                Toast.makeText(MainActivity.this,"Score reset Successfully",Toast.LENGTH_LONG).show();

            }
        });

        rLevel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = MainActivity.sharedpreferences.edit();
                editor.putInt("stage",1);
                editor.commit();

                Toast.makeText(MainActivity.this,"Level reset Successfully",Toast.LENGTH_LONG).show();
            }
        });

        sensor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(sharedpreferences.getInt("sensor",0)==0){
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("sensor",1);
                    editor.commit();
                    sensor.setText("Sensor is On");
                    senSensorManager.registerListener(MainActivity.this, senAccelerometer , SensorManager.SENSOR_DELAY_NORMAL);



                }
                else if(sharedpreferences.getInt("sensor",0)==1){
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("sensor",0);
                    editor.commit();
                    sensor.setText("Sensor is Off");
                    senSensorManager.unregisterListener(MainActivity.this);

                }


            }
        });










      //  myDialogue.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        myDialogue.show();
    }

    @Override
    protected void onPause() {
        sp.stop();
        super.onPause();
        senSensorManager.unregisterListener(this);
        Board.updateHighScore(Score.score);
        if(view==1)
        {
            MainActivity.pause=true;}



    }


    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {

        Sensor mySensor = sensorEvent.sensor;

        if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER) {

            float x = sensorEvent.values[0];


            if(view!=0){
            Bar.move(x);
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}