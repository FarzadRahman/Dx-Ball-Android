package com.example.ball.dx.dxball;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Farzad on 12/11/2017.
 */

public class Score {
    static int t=0,life,score=0;
    static boolean gameOver=false;


    Timer time=new Timer();

    TimerTask task=new TimerTask() {
        @Override
        public void run() {

            if(Score.t>0 && !MainActivity.pause)
                Score.t--;
            if(Score.t<=0 || Score.life <=0 || MainActivity.view==0){
                Score.gameOver=true;
                time.cancel();
            }
        }
    };

    public Score(int time, int life){
        Score.t=time;
        Score.life=life;
        this.time.scheduleAtFixedRate(task,1000,1000);

    }






}
