package com.example.ball.dx.dxball;

import android.content.Context;

import android.media.MediaPlayer;


/**
 * Created by Farzad on 12/27/2017.
 */

public class SoundPlayer {

    MediaPlayer mp,brickColide,dead;
    Context context;
    public SoundPlayer(Context context){
        this.context=context;

       mp = MediaPlayer.create(context, R.raw.background);





    }

    public void colide(){
        brickColide=MediaPlayer.create(context,R.raw.brick);
        brickColide.start();
    }

    public void dead(){
        dead=MediaPlayer.create(context,R.raw.explode);
        dead.start();
    }

    public void play(){

       // mp.start();
    }

    public  void stop(){
        mp.stop();
    }

}
