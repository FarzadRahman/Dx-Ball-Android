package com.example.ball.dx.dxball;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;

public class GameCanvas extends View {

    Paint paint;
    float x=0,y=0,dx=-10,dy=-10,r=40,Rx,Ry;
    boolean firstTime=true;
    public void setBarValue(int keyCode)
    {
        if(keyCode==KeyEvent.KEYCODE_DPAD_LEFT)
            this.x+=2;

    }
    protected void calculateNextPos(){

    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        float tx,ty;
        tx=event.getX();
        ty=event.getY();




        if( tx>=Rx && tx<=Rx+160){

            Rx=event.getX()-75;

        }

        return true;
    }

    protected void onDraw(Canvas canvas) {
        if(firstTime)
        {
            firstTime=false;
            x=canvas.getWidth()/2;
            y=canvas.getHeight()/2;
            Rx=canvas.getWidth()/2;
            Ry=canvas.getHeight()*93/100;
        }
        //calculateNextPos();

        canvas.drawRGB(255, 255, 255);
        paint.setColor(Color.RED);
        paint.setStyle(Style.FILL);
        canvas.drawCircle(x,y,r, paint);
        x+=dx; y+=dy;
        //left border
        if(x<=r){
            dx=-dx;

        }

        //brick

        else if(y+r>=Ry && x+r>=Rx && x<=Rx+160){
            dy=-dy;

        }

        //Top border

        else if(y<=r){
         dy=-dy;



        }

        //right border

        else if(canvas.getWidth()<=x+r){
            dx=-dx;


        }

        //Bottom border

        else if(canvas.getHeight()<=y+r){
             //dy=-dy;
             x=Rx+100;
             y=Ry-20-r;


        }
        //canvas.drawRect(100, 100, 200, 200, paint);
        paint.setColor(Color.BLUE);
        canvas.drawRect(Rx,Ry,Rx+150,Ry+20,paint);
        invalidate();
    }

    public GameCanvas(Context context) {
        super(context);
        paint = new Paint();
    }

}