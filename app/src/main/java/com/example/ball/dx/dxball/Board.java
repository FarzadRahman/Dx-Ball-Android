package com.example.ball.dx.dxball;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.support.constraint.solver.widgets.Rectangle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Queue;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by Farzad on 11/30/2017.
 */


public class Board extends View {

    Paint paint,Bpaint,tPaint;
    Stage stage;
    Queue<GameDrawable> drawable;
    List<Ball> ballDrawable;
    int totalBrick,level;

    static boolean showMenuButton=false;
    static Rect topBar;


    public Board(Context context) {
        super(context);
        Stage.totalBrick=0;
        LoadStage();
        int height=Resources.getSystem().getDisplayMetrics().heightPixels;
        int width=Resources.getSystem().getDisplayMetrics().widthPixels;
        paint=new Paint();
        Bpaint=new Paint();
        tPaint=new Paint();
        tPaint.setTextSize(width*5/100);
        tPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
        paint.setAntiAlias(true);
        paint.setColor(Color.RED);
        paint.setShadowLayer(4.5f, 5.0f, 5.0f, 0x80000000);
        paint.setTextSize(50);
        setLayerType(LAYER_TYPE_SOFTWARE, paint);

        Bpaint.setStyle(Paint.Style.STROKE);
        Bpaint.setColor(Color.BLACK);

        MainActivity.pause=false;

        //canvas.drawRect(0,canvas.getHeight()*4/100,canvas.getWidth(),(canvas.getHeight()*4/100)+5,tPaint);
        topBar=new Rect();
        topBar.left=0;
        topBar.top=height*4/100;
        topBar.right=width;
        topBar.bottom=topBar.top+5;








    }

    public void reloadStage(int stage){
        level=stage;
        for (Ball b: ballDrawable){
            b.run=false;
        }

        SharedPreferences.Editor editor = MainActivity.sharedpreferences.edit();
        editor.putInt("stage",level);
        editor.commit();


        //InputStream in_s = getResources().openRawResource(R.raw.stage2);

        InputStream in_s = getResources().openRawResource(getResources().getIdentifier("stage"+String.valueOf(level),"raw","com.example.ball.dx.dxball"));

        byte[] b = new byte[0];
        try {
            b = new byte[in_s.available()];
            in_s.read(b);
        } catch (Exception e) {
            e.printStackTrace();
        }


        String lines[] = new String(b).split("\\r?\\n");
        Log.d("text", String.valueOf(lines.length));



        this.stage=new Stage(lines);


        drawable=new ConcurrentLinkedQueue<>();
        for(GameDrawable gd: this.stage.drawable){
            drawable.add(gd);

        }
        //drawable = (Queue<GameDrawable>) stage.drawable;
        ballDrawable=this.stage.ballDrawable;
        Toast.makeText(getContext(),"You are on level "+ String.valueOf(level),Toast.LENGTH_LONG).show();
        Score.gameOver=false;
        this.totalBrick=Stage.totalBrick;



    }


    public void LoadStage() {

        level = MainActivity.sharedpreferences.getInt("stage",1);
        if(level==0){
            level=1;
        }

        Log.d("stage",String.valueOf(level));



       InputStream in_s = getResources().openRawResource(getResources().getIdentifier("stage"+String.valueOf(level),"raw","com.example.ball.dx.dxball"));



        //InputStream in_s = getResources().openRawResource(R.raw.stage1);

        byte[] b = new byte[0];
        try {
            b = new byte[in_s.available()];
            in_s.read(b);
        } catch (IOException e) {
            e.printStackTrace();
        }


        String lines[] = new String(b).split("\\r?\\n");
        Log.d("text", String.valueOf(lines.length));



        stage = new Stage(lines);
        drawable=new ConcurrentLinkedQueue<>();
        for(GameDrawable gd: stage.drawable){
            drawable.add(gd);

        }
        //drawable = (Queue<GameDrawable>) stage.drawable;
        ballDrawable=stage.ballDrawable;

        this.totalBrick=Stage.totalBrick;
        Toast.makeText(getContext(),"You are on level "+ String.valueOf(level),Toast.LENGTH_LONG).show();

    }

    static void updateHighScore(int currentScore){

        int defaultScore = MainActivity.sharedpreferences.getInt("game",0);
        if(currentScore>defaultScore){

            SharedPreferences.Editor editor = MainActivity.sharedpreferences.edit();
            editor.putInt("game",currentScore);
            editor.commit();


        }



    }

    public void backButton(Canvas canvas){

        Board.showMenuButton=true;
        int left=canvas.getWidth()*38/100,top=canvas.getHeight()*56/100;
        Rect r=new Rect(left,top,canvas.getWidth()*67/100,canvas.getHeight()*63/100);
        canvas.drawRect(r,tPaint);
        Paint p=new Paint();
        p.setColor(Color.WHITE);
        p.setTextSize(canvas.getWidth()*5/100);
        p.setStyle(Paint.Style.FILL_AND_STROKE);
        canvas.drawText("Main Menu", canvas.getWidth()*38/100, canvas.getHeight()*60/100,p);


    }





    @Override
    protected void onDraw(Canvas canvas) {

        if(Board.showMenuButton==true){
            Board.showMenuButton=false;
        }


        canvas.drawRGB(0,0,128);
        //for changing level
        Log.d("Bricks",String.valueOf(totalBrick));
        if(totalBrick<=0){
            Score.score=0;
            Score.t=0;
            Stage.totalBrick=10;
            reloadStage(level+1);

        }

        //write pause
        if(MainActivity.pause){
            canvas.drawText("Paused", canvas.getWidth()*40/100, canvas.getHeight()/2, paint);
            Paint p=new Paint();
            p.setColor(Color.WHITE);
            p.setTextSize(40);
            canvas.drawText("press back button to resume", canvas.getWidth()*20/100, canvas.getHeight()*70/100, p);

            backButton(canvas);

        }


        //write game over
        if(Score.gameOver){
            Board.updateHighScore(Score.score);

            canvas.drawText("Game Over", canvas.getWidth()*35/100, canvas.getHeight()/2, paint);

            backButton(canvas);

        }

        //set score
        tPaint.setColor(Color.CYAN);
        canvas.drawText("Score: "+ String.valueOf(Score.score), 10, canvas.getHeight()/40, tPaint);
        //canvas.drawText("brick left: "+ String.valueOf(totalBrick), 10, canvas.getHeight()/40, tPaint);

        //set Time & Life
        tPaint.setColor(Color.GREEN);
        canvas.drawText("⏳:"+String.valueOf(Score.t),canvas.getWidth()*45/100,canvas.getHeight()/34,tPaint);
        tPaint.setColor(Color.RED);
        canvas.drawText("♥:"+Score.life,canvas.getWidth()*88/100,canvas.getHeight()/40,tPaint);


        canvas.drawRect(topBar,tPaint);


        //running game
        if(!MainActivity.pause && !Score.gameOver) {

            for (Ball b: ballDrawable){

                canvas.drawCircle((int) b.getPostion().get("x"), (int) b.getPostion().get("y"), (int) b.getPostion().get("r"), b.getPaint());

                if(!b.run){
                    b.run=true;
                new Thread(b).start();}

            }

            for (GameDrawable dr : drawable) {

                //for bar
                //super.onDraw(canvas);
                if (dr.type() == 2) {
                    float x, y, height, width;
                    x = (Float) dr.getPostion().get("x");
                    y = (Float) dr.getPostion().get("y");
                    height = (Float) dr.getPostion().get("height");
                    width = (Float) dr.getPostion().get("width");

                    canvas.drawRect(x, y, x + width, y + height, dr.getPaint());
                    canvas.drawRect(x+5, y+5, x + width-5, y + height-5, tPaint);

                }
                //for brick
                else if(dr.type()==3){


                    canvas.drawRect((Rect) dr.getPostion().get("rect"),dr.getPaint());
                    canvas.drawRect((Rect) dr.getPostion().get("rect"),Bpaint);




                    int brickX=(int) dr.getPostion().get("x");
                    int brickY=(int) dr.getPostion().get("y");
                    int brickR=(int) dr.getPostion().get("right");
                    int brickB=(int) dr.getPostion().get("bottom");



                    for (Ball b: ballDrawable){

                        if(b.colidesWith(brickX,brickY,brickR,brickB)){
                            int n=(int)dr.getPostion().get("type");
                            Log.d("type",String.valueOf(n));
                            MainActivity.sp.colide();
                            if(n==1){
                                drawable.remove(dr);
                                Score.score+=5;
                                this.totalBrick-=1;
                            }
                            else {
                                //type -1

                                try {
                                    dr.runx();
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }

                                break;
                            }

                        }


                    }

                }


            }
        }
        invalidate();


    }




}
