package com.example.ball.dx.dxball;

import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Farzad on 12/4/2017.
 */

public class Brick implements GameDrawable,Runnable {

    int srceenWidth,x,y,right,bottom;
    Paint paint;
    Map position;
    Rect r;
    public static float width=0;
    int type=0;


    public Brick(int x, int y, float screenW, int type){
        this.type=type;
        paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        hardness();

        int right= (int) (screenW/6);
        this.right=x+right;
        Brick.width=this.right;
        int bottom= Resources.getSystem().getDisplayMetrics().heightPixels/20;
        this.bottom=y+bottom;
        this.x=x;
        this.y=y;
        r = new Rect(x, y, x+right, y+bottom);
        position=new HashMap<String, Float>();


    }



    @Override
    public int type() {
        return 3;
    }

    @Override
    public Paint getPaint() {
        return paint;
    }

    public void hardness(){
        switch (this.type){
            case 1:
                paint.setColor(Color.MAGENTA);
                break;
            case 2:
                paint.setColor(Color.GRAY);
                break;
            case 3:
                paint.setColor(Color.RED);


        }


    }



    @Override
    public void runx() {
        //new Thread(this).start();
        type=type-1;
        Log.d("type","called");
        hardness();
        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }

    @Override
    public Map getPostion() {

        position.put("rect",r);
        position.put("x",x);
        position.put("y",y);
        position.put("right",right);
        position.put("bottom",bottom);
        position.put("type",this.type);

        return position;
    }

    @Override
    public void run() {
        type=type-1;
        Log.d("type","called");
        hardness();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }
}
